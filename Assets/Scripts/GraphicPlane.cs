﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GraphicPlane : MonoBehaviour
{
    public GameObject labelPrefab;
    public GameObject linePrefab;
    public GameObject coordinatelinePrefab;
    static GraphicPlane instance;

    private void Awake()
    {
        instance = this;
        for(int i = 1; i < 100; i++)
        {
            TextMeshPro tmp = Instantiate(labelPrefab, transform).GetComponent<TextMeshPro>();
            tmp.transform.localPosition = new Vector3(-0.5f, i,0);
            tmp.text = i.ToString("00");
            LineRenderer line = Instantiate(coordinatelinePrefab, transform).GetComponent<LineRenderer>();
            line.SetPosition(0,new Vector3(0,i,-100));
            line.SetPosition(1,new Vector3(0,i,100));
            tmp = Instantiate(labelPrefab, transform).GetComponent<TextMeshPro>();
            tmp.transform.localPosition = new Vector3(i, -0.5f,0);
            tmp.text = i.ToString("00");
            line = Instantiate(coordinatelinePrefab, transform).GetComponent<LineRenderer>();
            line.SetPosition(0,new Vector3(0,-100, i));
            line.SetPosition(1,new Vector3(0,100, i));
        }
    }

    public static void DrawPoint(float x0, float y0, float x1, float y1, Color c)
    {
        instance.drawPoint(x0,y0,x1,y1,c);
    }
    void drawPoint(float x0, float y0, float x1, float y1, Color c)
    {
        LineRenderer line = Instantiate(linePrefab, transform).GetComponent<LineRenderer>();
        line.SetPosition(0,new Vector3(0,y0,x0));
        line.SetPosition(1,new Vector3(0,y1,x1));
        line.material.color = c;
    }
}
