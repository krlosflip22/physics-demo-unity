﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceDetection : MonoBehaviour
{

    void Awake()
    {
        Debug.Log($"InitialForce: {GetComponent<Rigidbody>().mass * Physics.gravity}");
    }

    private void OnCollisionEnter(Collision other)
    {
        Vector3 collisionForce = other.impulse / Time.fixedDeltaTime;

        Debug.Log($"CollisionForce: {collisionForce}");
        Debug.Log($"TotalForce: {collisionForce + GetComponent<Rigidbody>().mass * Physics.gravity}");
    }

    private void OnCollisionStay(Collision other)
    {
        Vector3 collisionForce = other.impulse / Time.fixedDeltaTime;

        Debug.Log($"CollisionForce onStay: {collisionForce}");
        Debug.Log($"TotalForce: {collisionForce + GetComponent<Rigidbody>().mass * Physics.gravity}");
    }
}
