﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementExample : MonoBehaviour
{
    public enum MovementMode
    {
        MRU,
        MRUV,
        MCU,
        MCUV
    }

    public MovementMode movMode;

    [Header("MRU")]
    public Vector3 initialPosition = Vector3.right;
    public Vector3 speed = Vector3.right;


    [Header("MRUV")]
    public Vector3 initialSpeed = Vector3.zero;
    public Vector3 accel = Vector3.right;

    [Header ("MCU")]
    public float initialAngle;
    public float currentAngle;
    public float angularSpeed;
    public float radius;
    [Header ("MCUV")]
    public float initialAngularSpeed;
    public float angularAccel;

    Vector3 prevSpeed;
    Vector3 prevPosition;
    float prevAngle;
    float prevAngSpeed;
    float previousTime;

    [Header("Debug")]
    public bool showGraphics;
    public Color speedLineColor = Color.green;
    public Color positionLineColor = Color.red;

    float currentTime;

    event System.Action movementExec;

    void Awake()
    {
        transform.position = initialPosition;
        currentAngle = initialAngle;

        prevPosition = transform.position;
        prevAngle = currentAngle;

        prevSpeed = initialSpeed;
        prevAngSpeed = initialAngularSpeed;

        switch(movMode)
        {
            case MovementMode.MRU: movementExec += MRUExec; break;
            case MovementMode.MRUV: movementExec += MRUVExec; break;
            case MovementMode.MCU: movementExec += MCUExec; break;
            case MovementMode.MCUV: movementExec += MCUVExec; break;
        }
    }

    bool countStarted = false;


    private void FixedUpdate()
    {
        movementExec?.Invoke();
    }

    float ToRadians(float angle)
    {
        return angle * Mathf.PI / 180;
    }

    void MRUExec()
    {
        currentTime = Time.time;
        //     x(t)        =     x0          +   v   *     t
        transform.position = initialPosition + speed * currentTime;

        if(countStarted) return;

        StartCoroutine(DrawGraphics());
        countStarted = true;
    }

    void MRUVExec()
    {
        currentTime = Time.time;
        //v(t)=      v0      +   a   *      t
        speed = initialSpeed + accel * currentTime;
        //     x(t)        =        x0       +        v0    *       t     +  0.5 *  a   * (          t^2          )
        transform.position = initialPosition + initialSpeed * currentTime + .5f * accel * currentTime * currentTime;

        if(countStarted) return;

        StartCoroutine(DrawGraphics());
        countStarted = true;
    }

    void MCUExec()
    {
        currentTime = Time.time;
        //  θ(t)     =      θ0      +      w       *      t
        currentAngle = initialAngle + angularSpeed * currentTime;

        transform.position = radius * new Vector3(Mathf.Cos(ToRadians(currentAngle)), Mathf.Sin(ToRadians(currentAngle)),0);

        if(countStarted) return;

        StartCoroutine(DrawGraphics());
        countStarted = true;
    }

    void MCUVExec()
    {
        currentTime = Time.time;
        //  w(t)     =         w0          +      a       *      t
        angularSpeed = initialAngularSpeed + angularAccel * currentTime;
        //  θ(t)     =      θ0      +      w       *      t
        currentAngle = initialAngle + initialAngularSpeed * currentTime + .5f * angularAccel * currentTime * currentTime;

        transform.position = radius * new Vector3(Mathf.Sin(ToRadians(currentAngle)), Mathf.Cos(ToRadians(currentAngle)),0);

        if(countStarted) return;

        StartCoroutine(DrawGraphics());
        countStarted = true;
    }

    IEnumerator DrawGraphics()
    {
        if(showGraphics)
        {
            for(;;)
            {
                Debug.Log($"Time: {currentTime} Position: {transform.position.x} Speed: {speed}");

                //x vs t
                GraphicPlane.DrawPoint(previousTime, prevPosition.x, currentTime, transform.position.x, positionLineColor);

                if(movMode == MovementMode.MRU || movMode == MovementMode.MRUV)
                {
                    //v vs t
                    GraphicPlane.DrawPoint(previousTime, prevSpeed.x, currentTime, speed.x, speedLineColor);
                    prevSpeed = speed;
                }
                else
                {
                    //v vs t
                    GraphicPlane.DrawPoint(previousTime, prevAngSpeed, currentTime, angularSpeed, speedLineColor);
                    prevAngSpeed = angularSpeed;
                }

                prevPosition = transform.position;
                previousTime = currentTime;

                yield return new WaitForSeconds(.25f);
            }
        }

    }

}
