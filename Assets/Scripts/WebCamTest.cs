﻿using UnityEngine;
using UnityEngine.UI;

public class WebCamTest : MonoBehaviour
{
    public RawImage rawimage;
    void Start ()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
            Debug.Log("Camera: " + i + " " + devices[i].name);

        WebCamTexture webcamTexture = new WebCamTexture();
        webcamTexture.deviceName = devices[1].name;
        //webcamTexture.width = 1280;
        //webcamTexture.height = 720;
        rawimage.texture = webcamTexture;
        rawimage.material.mainTexture = webcamTexture;
        webcamTexture.Play();
    }
}